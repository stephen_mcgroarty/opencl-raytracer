//
//  Application.cpp
//  OpenCLRaytracerTest
//
//  Created by Stephen Mcgroarty on 20/03/2014.
//  Copyright (c) 2014 Stephen Mcgroarty. All rights reserved.
//

#include "Application.h"
#include "Vector.h"
#include "Camera.h"
#include <ctime>
#include <sstream>
void Application::WriteBMP(const std::string & filePath, const Colour * inputData,const unsigned int w,const unsigned int h) const
{
    
    Colour *data = new Colour[w*h];
    
    //Invert the image
    for( unsigned int scanline = 1; scanline <= h; scanline++)
    {
        for(unsigned int xPixel = 0; xPixel < w; xPixel++)
        {
            unsigned int iPixel = xPixel + w*(scanline-1);
            data[iPixel] = inputData[(w*scanline) - xPixel];
        }
    }
    
    
    const unsigned int imageSize = 3*w*h;
	const unsigned int size  = imageSize + 54; // 54 bits for the header
    Byte header[54];
    
    for(int i =0; i < 54; i++)
        header[i] = 0;
    
    header[0] = 'B';
    header[1] = 'M';
    
    header[2] = (Byte)size;
    header[3] = (Byte)(size>>8);
    header[4] = (Byte)(size>>16);
    header[5] = (Byte)(size>>24);
    
    header[10] = (Byte)54;
    header[14] = 40; // start of info header
    
    header[18] = (Byte)(w);
    header[19] = (Byte)(w>>8);
    header[20] = (Byte)(w>>16);
    header[21] = (Byte)(w>>24);
    
    header[22] = (Byte)(h);
    header[23] = (Byte)(h>>8);
    header[24] = (Byte)(h>>16);
    header[25] = (Byte)(h>>24);
    
	header[26] = 1;
	header[28] = 24;
    
	header[35] = (Byte)(imageSize);
	header[36] = (Byte)(imageSize>>8);
	header[37]=  (Byte)(imageSize>>16);
	header[38]=  (Byte)(imageSize>>24);
    
    const char * str = (filePath).c_str();
    
    FILE * file = fopen( str, "wb");
    
    fwrite(header, sizeof(Byte), 54, file);
	for(unsigned int i =0; i < w*h; i++)
	{
		Byte colour[3];
		colour[0] = data[i].blue;
		colour[1] = data[i].green;
		colour[2] = data[i].red;
		fwrite(colour,1,3,file);
	}
    
    fclose(file);
    delete[] data;
}



void Application::Init(unsigned int width, unsigned int height)
{
    
    m_Running        = true;
    m_Width          = width;
    m_Height         = height;
    m_SnapshotWidth  = width*3;
    m_SnapshotHeight = height*3;
    m_MSAA           = 2;
    m_SnapshotMSAA   = 4;
    m_Dx             = 0.0f;
    m_Direction      = true;
    m_TakingSnapshot = false;
    m_pImageBuffer   = new Colour[m_Width*m_Height];
    m_pSnapShotBuffer= new Colour[m_SnapshotWidth*m_SnapshotHeight];
    
    
    InitSDL();
  

    float m_Aspect = static_cast<float>(m_Width)/m_Height;
    
    
    Vector cameraEye(0.0f,0.0f,7.5f);
    
    m_Camera =  new Camera(cameraEye, Vector(0.0f,0.0f,0.0f) - cameraEye, Vector(0.0f,1.0f,0.0f),m_Aspect,m_Width,m_Height);

    m_CL.Init();
    m_CL.CreateQueue("Main", OpenCLWrapper::GPU);
    m_CL.CreateQueue("Secondary", OpenCLWrapper::CPU);

    m_CL.LoadProgram(FILE_PATH + "Raytracer.cl","-D MAX_DEPTH=5");
    
    m_CL.CreateImage2D("Framebuffer", CL_MEM_WRITE_ONLY, m_Width, m_Height, nullptr);
    m_CL.CreateImage2D("snapshot", CL_MEM_WRITE_ONLY, m_SnapshotWidth, m_SnapshotHeight, nullptr);
    
    m_CL.SetKernalArgs(0,"Framebuffer");

    
    m_Camera->SendToKernal(m_CL);
    
    


    m_CL.SetKernalArgs(7, sizeof(m_Dx), &m_Dx);
    m_CL.SetKernalArgs(8, sizeof(m_MSAA), &m_MSAA);

    
}


void Application::Update()
{
    if( m_Direction )
        m_Dx += 0.1f;
    else
        m_Dx -= 0.1f;
    
    if( m_Dx > 4.0f || m_Dx < 0 )
        m_Direction = !m_Direction;
    
    m_CL.SetKernalArgs(7, sizeof(m_Dx), &m_Dx);

    
    SDL_Event e;
	while( SDL_PollEvent(&e) )
	{
		if(e.type == SDL_QUIT )
			m_Running = false;
	}
    
    
    const Uint8 * keys = SDL_GetKeyboardState(NULL);
    
    if( keys[SDL_SCANCODE_D] )
    {
        m_Camera->MoveRight(0.1f);
        m_Camera->SendToKernal(m_CL);
    }
    
    if( keys[SDL_SCANCODE_A] )
    {
        m_Camera->MoveRight(-0.1f);
        m_Camera->SendToKernal(m_CL);
    }
    
    if( keys[SDL_SCANCODE_W] )
    {
        m_Camera->MoveForward(0.1f);
        m_Camera->SendToKernal(m_CL);
    }
    
    if( keys[SDL_SCANCODE_S] )
    {
        m_Camera->MoveForward(-0.1f);
        m_Camera->SendToKernal(m_CL);
    }
    
    if( keys[SDL_SCANCODE_Z] )
    {
        m_Camera->MoveUp(-0.1f);
        m_Camera->SendToKernal(m_CL);
    }
    
    if( keys[SDL_SCANCODE_X] )
    {
        m_Camera->MoveUp(0.1f);
        m_Camera->SendToKernal(m_CL);
    }
    
    if( keys[SDL_SCANCODE_UP])
    {
    
        if( m_MSAA != 8 )
        {
            m_MSAA += 2;
            m_CL.SetKernalArgs(8, sizeof(m_MSAA), &m_MSAA);
        }
    }
    if( keys[SDL_SCANCODE_DOWN])
    {
        if( m_MSAA != 2 )
        {
            m_MSAA -= 2;
            m_CL.SetKernalArgs(8, sizeof(m_MSAA), &m_MSAA);
        }
    }
    
    // Press Space to take a snapshot
    if( keys[SDL_SCANCODE_SPACE])
    {
        if( !m_TakingSnapshot ) //Check not already inprogress
        {
            
            printf("Taking snapshot at resolution %d*%d and MSAA level of %d*%d\n",m_SnapshotWidth,m_SnapshotHeight,m_SnapshotMSAA,m_SnapshotMSAA);
            const size_t globalWorkSize[3] = {m_SnapshotWidth,m_SnapshotHeight,1};
            const size_t offset[3] = {0,0,0};

            
            
            m_CL.SetKernalArgs(0,"snapshot"); // Save result to snapshot
            m_CL.SetKernalArgs(8, sizeof(m_SnapshotMSAA), &m_SnapshotMSAA);


            m_CL.EnqueueNDRangeKernel("Secondary", 2, globalWorkSize,nullptr,&m_ExecuteEvent);
            m_CL.ReadImage("Secondary","snapshot", offset, globalWorkSize,m_pSnapShotBuffer ,false,&m_ExecuteEvent,1,&m_ReadEvent);
            m_TakingSnapshot = true;
            
            m_CL.SetKernalArgs(0,"Framebuffer"); // Reset save location to the framebuffer
            m_CL.SetKernalArgs(8, sizeof(m_MSAA), &m_MSAA);


        } else // If already taking a snapshot
        {
            cl_int *status = static_cast<cl_int*>(m_CL.GetEventInfo(&m_ExecuteEvent, CL_EVENT_COMMAND_EXECUTION_STATUS));
            
            if( *status == CL_RUNNING )
                printf("Running\n");
            else if( *status == CL_SUBMITTED )
                printf("Submitted\n");
            else if( *status == CL_QUEUED )
                printf("Queued\n");
            else if( *status == CL_COMPLETE )
                printf("Completed\n");
            
            free(status);
        }
    }
    
    
    if( m_TakingSnapshot )
    {
        cl_int *status = static_cast<cl_int*>(m_CL.GetEventInfo(&m_ReadEvent, CL_EVENT_COMMAND_EXECUTION_STATUS));
    
        if( *status ==CL_COMPLETE )
        {
            printf("Snapshot taken, now saving bitmap\n");
            std::stringstream name;
            name << FILE_PATH << "Snapshot" << ++m_SnapshotNumber << ".bmp";
        
            printf("Saved to location %s\n",name.str().data());
        
            WriteBMP(name.str(), m_pSnapShotBuffer, m_SnapshotWidth, m_SnapshotHeight);

            m_TakingSnapshot = false;
        }
        if( status ) free(status);

    }
    
    
}

void Application::InitSDL()
{
    SDL_Init(SDL_INIT_VIDEO);
    
    m_pWindow = SDL_CreateWindow("OpenCL Raytracer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_Width, m_Height, SDL_WINDOW_SHOWN);
    m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, SDL_RENDERER_ACCELERATED);
    m_ScreenRect.x = m_ScreenRect.y = 0;
    m_ScreenRect.w = m_Width;
    m_ScreenRect.h = m_Height;
}




Application::~Application()
{
    if( !m_pImageBuffer ) delete[] m_pImageBuffer;
    if( !m_pSnapShotBuffer)delete[]m_pSnapShotBuffer;
    SDL_DestroyWindow(m_pWindow);
    SDL_Quit();
}



void Application::Render() 
{
    const size_t globalWorkSize[3] = {m_Width,m_Height,1};
    
    
    m_CL.EnqueueNDRangeKernel("Main",2,globalWorkSize);
    
    const size_t offset[3] = {0,0,0};
    m_CL.ReadImage("Main","Framebuffer", offset, globalWorkSize,m_pImageBuffer,true,nullptr,0,nullptr);

    
    Uint32 rmask = 0x000000ff;
    Uint32 gmask = 0x0000ff00;
    Uint32 bmask = 0x00ff0000;
    Uint32 amask = 0xff000000;
    
    
   
    SDL_Surface * surface = SDL_CreateRGBSurfaceFrom(m_pImageBuffer, m_Width, m_Height, 32, m_Width*4, rmask, gmask, bmask, amask);
    SDL_Texture * texture = SDL_CreateTextureFromSurface(m_pRenderer, surface);
    SDL_RenderClear(m_pRenderer);
    SDL_RenderCopyEx(m_pRenderer, texture, NULL, &m_ScreenRect,0,NULL,(SDL_RendererFlip)(SDL_FLIP_VERTICAL | SDL_FLIP_HORIZONTAL));
    SDL_RenderPresent(m_pRenderer);
    
    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surface);
    //WriteBMP(FILE_PATH + "test.bmp", m_pImageBuffer, m_Width, m_Height);
}