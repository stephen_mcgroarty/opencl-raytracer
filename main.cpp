//
//  main.cpp
//  OpenCLRaytracerTest
//
//  Created by Stephen Mcgroarty on 11/03/2014.
//  Copyright (c) 2014 Stephen Mcgroarty. All rights reserved.
//

#include "Application.h"

int main(int argc, const char * argv[])
{

    
    const unsigned int WIDTH = 640,HEIGHT = 480;
   
    Application app;
    
    app.Init(WIDTH, HEIGHT);

    while (app.IsRunning() )
    {
        app.Update();
        app.Render();

    }
    
    return 0;
}

