

__kernel void Raytracer(__write_only image2d_t image)
{
    int2 position = { get_global_id(0),get_global_id(1) };
    
    write_imagef(image,position,(float4)(1.0f,0.0f,0.0f,1.0f));
    
}