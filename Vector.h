//
//  Vector.h
//  RaytracerTake2
//
//  Created by Stephen Mcgroarty on 19/11/2013.
//  Copyright (c) 2013 Stephen Mcgroarty. All rights reserved.
//

#ifndef __RaytracerTake2__Vector__
#define __RaytracerTake2__Vector__

#include <string>
#include "Common.h"


class Vector
{
public:
    Vector();
    Vector(const FLOAT x,const FLOAT y,const FLOAT z);
    

    const FLOAT getX() const {return x; }
    const FLOAT getY() const {return y; }
    const FLOAT getZ() const {return z; }
    
    const FLOAT magnitude() const;
    
    const Vector negative() const;
    const Vector normalize() const;

    
    const FLOAT dotProduct(const Vector& ) const;
    const Vector crossProduct(const Vector &) const;
    
    const Vector operator-(const Vector &) const;
    void operator-=(const Vector & );
    
    const Vector operator+(const Vector &) const ;
    void operator+=(const Vector &);
    
    const Vector operator*(const FLOAT )const ;
    void operator*=(const FLOAT);
    void operator*=(const Vector &);
    
    
    const Vector reflect(const Vector & normal) const;

    const std::string toString() const;
    
    
    FLOAT distance(const Vector &) const;
    
private:
    FLOAT x,y,z;
    
};




static const Vector Y_AXIS(0.0,1.0,0.0);
static const Vector X_AXIS(1.0,0.0,0.0);
static const Vector Z_AXIS(0.0,0.0,1.0);



#endif /* defined(__RaytracerTake2__Vector__) */
