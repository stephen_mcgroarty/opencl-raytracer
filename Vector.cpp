//
//  Vector.cpp
//  RaytracerTake2
//
//  Created by Stephen Mcgroarty on 19/11/2013.
//  Copyright (c) 2013 Stephen Mcgroarty. All rights reserved.
//

#include "Vector.h"
#include <cmath>
#include <sstream>


Vector::Vector(): x(0.0f),y(0.0f),z(0.0f)
{}

Vector::Vector(const FLOAT ix,const FLOAT iy, const FLOAT iz): x(ix),y(iy),z(iz)
{}

const FLOAT Vector::magnitude() const
{
    return sqrt(x*x + y*y + z*z);
}

const Vector Vector::crossProduct(const Vector &vec) const
{
    return Vector( y*vec.getZ() - z*vec.getY(), x*vec.getZ() - z*vec.getX(), x*vec.getY() + y*vec.getX());
}

const FLOAT Vector::dotProduct(const Vector &vec) const
{
    return vec.x*x + y*vec.y + z*vec.z;
}

const Vector Vector::operator+(const Vector& vec) const
{
    return Vector(x+vec.getX(),y+vec.getY(),z+vec.getZ());
}


void Vector::operator+=(const Vector& vec)
{
    x += vec.getX();
    y += vec.getY();
    z += vec.getZ();
}


const Vector Vector::operator-(const Vector& vec) const
{
    return Vector(x-vec.getX(),y-vec.getY(),z-vec.getZ());
}


void Vector::operator-=(const Vector& vec)
{
    x -= vec.getX();
    y -= vec.getY();
    z -= vec.getZ();
}


const Vector Vector::operator*(const FLOAT f) const
{
    return Vector(x*f,y*f,z*f);
}


void Vector::operator*=(const FLOAT f)
{
    x *= f;
    y *= f;
    z *= f;
}



const Vector Vector::negative() const
{

    return Vector() - *this;
}


const Vector Vector::normalize() const
{
    float mag = magnitude();
    
    if( mag != 0.0f && mag != 1.0f)
        return Vector(x/mag,y/mag,z/mag);
    else
        return *this;
}


const std::string Vector::toString() const
{
    
    std::ostringstream stream;
    stream << " X: " << x << " Y: " << y << " Z: " << z;
    return stream.str();
}


FLOAT Vector::distance(const Vector & vec) const
{
    return (*this -vec).magnitude();
}


void Vector::operator*=(const Vector & rhs)
{
    x*= rhs.getX();
    y*= rhs.getY();
    z*= rhs.getZ();
}


const Vector Vector::reflect(const Vector &normal) const
{
    
    return *this - (normal* 2 * this->dotProduct(normal));
    
}
