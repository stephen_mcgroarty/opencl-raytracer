//
//  OpenCLWrapper.h
//  OpenCL
//
//  Created by Stephen Mcgroarty on 06/03/2014.
//  Copyright (c) 2014 Stephen Mcgroarty. All rights reserved.
//

#ifndef __OpenCL__OpenCLWrapper__
#define __OpenCL__OpenCLWrapper__

#include <string>
#include <OpenCL/OpenCL.h>
#include <vector>
#include <map>

class OpenCLWrapper
{
public:
    
    //The type of device
    enum Type
    {
        GPU   = 0x01, //Try to use the GPU over CPU (only supports using a single GPU)
        CPU   = 0x02, //Try to use the CPU over GPU

    };
    
    ~OpenCLWrapper();
    void Init();
    
    
    std::string GetPlatformInfo(unsigned int index,cl_platform_info infoParam) const;
    
    void * GetDeviceInfo(unsigned int index,cl_device_info) const;
    void * GetEventInfo(cl_event *event,cl_event_info) const;
    
    void LoadProgram(const std::string & filepath, const char * args = nullptr);
    
    
    //Create a queue with the identifier passed in as param and the device type to run it on (GPU or CPU only)
    void CreateQueue(const std::string & name, Type );
    
    
    //To-do: Change cl_mem_flags to a 'proper' enum
    void CreateBuffer(const std::string & name,cl_mem_flags flags,unsigned int size, void *data);
    
    // Caller is reponsible for freeing returned memory
    void*ReadBuffer  (const std::string & queue,const std::string & name,unsigned int size,bool block);
    void WriteBuffer (const std::string & queue,const std::string & name,unsigned int size, const void * data,bool block);
    
    // Caller is reponsible for returned memory
    void*ReadImage(const std::string & queue,const std::string & name,const size_t offset[3],const size_t region[3],bool block);
    
    
    void ReadImage(const std::string & queue,
                   const std::string & name,
                   const size_t offset[3],
                   const size_t region[3],
                   void* data,
                   bool block,
                   const cl_event* list,
                   unsigned int numEventsInList,
                   cl_event * event);

    void CreateImage2D(const std::string & name, cl_mem_flags flags,size_t width, size_t height,void * data);
    
    void SetKernalArgs(unsigned int index,size_t size, const void * data);
    void SetKernalArgs(unsigned int index,const std::string & buffer);
    
    
    void EnqueueNDRangeKernel(const std::string & queue,unsigned int dimentions,const size_t globalWorkSize[3],const size_t localWorkSize[3] = nullptr,cl_event * event = nullptr);

private:
    
    std::string m_ProgramSource;
    
    
    //Stores all of the devices on the platform
    std::vector<cl_device_id> m_vDevices;
    unsigned int m_MainGPU; //Index for the primary GPU on the system
    unsigned int m_MainCPU; //Index for the primary CPU
    
    
    std::vector<cl_platform_id> m_vPlatforms;
    std::map<std::string,cl_command_queue> m_Queues;
    
    
    cl_program m_Program;
    cl_kernel m_Kernel;
    cl_context m_Context;
    
    
    std::map<std::string,cl_mem> m_Buffers;
};

#endif /* defined(__OpenCL__OpenCLWrapper__) */
