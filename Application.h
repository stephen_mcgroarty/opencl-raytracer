//
//  Application.h
//  OpenCLRaytracerTest
//
//  Created by Stephen Mcgroarty on 20/03/2014.
//  Copyright (c) 2014 Stephen Mcgroarty. All rights reserved.
//

#ifndef __OpenCLRaytracerTest__Application__
#define __OpenCLRaytracerTest__Application__

#include "OpenCLWrapper.h"
#include "Common.h"
#include "Camera.h"

#include <SDL2/SDL.h>


class Application
{
public:

    ~Application();
    void Init(unsigned int width,unsigned int height);
    
    
    void Render();
    void Update();
    
    bool IsRunning() const { return m_Running; }
    
private:
    
    //Width and height of the Framebuffer/SDL Window
    unsigned int m_Width,m_Height;
    
    //Width and height of the outputed screen shot
    unsigned int m_SnapshotWidth,m_SnapshotHeight;
    
    unsigned int m_SnapshotNumber; //The number of snapshots taken
    
    float m_Aspect;
    
    OpenCLWrapper m_CL;
    
    //Multi-sampling anti-aliasing level for the frame and for the snapshot respectively
    unsigned int m_MSAA,m_SnapshotMSAA;
    
    //Image buffer is the framebuffer
    Colour * m_pImageBuffer,*m_pSnapShotBuffer;
    
    bool m_Running;
    SDL_Window * m_pWindow;
    SDL_Renderer* m_pRenderer;
    SDL_Rect m_ScreenRect;

    void WriteBMP(const std::string & filePath, const Colour * data,const unsigned int w,const unsigned int h) const;

    Camera *m_Camera;
    float m_Dx;
    bool m_Direction;
    
    void InitSDL();
    
    
    bool m_TakingSnapshot;
    
    
    //Somewhat breaking the encapsulation of the OpenCLWrapper here
    cl_event m_ExecuteEvent,m_ReadEvent;
};

#endif /* defined(__OpenCLRaytracerTest__Application__) */
