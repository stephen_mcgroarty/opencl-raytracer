//
//  Camera.cpp
//  RaytracerTake2
//
//  Created by Stephen Mcgroarty on 23/11/2013.
//  Copyright (c) 2013 Stephen Mcgroarty. All rights reserved.
//

#include "Camera.h"

#define _USE_MATH_DEFINES
#include <cmath>

#define M_PI 3.141592654



Camera::Camera(const Vector & e,const Vector & view, const Vector & up,FLOAT ASPECT,const int WIDTH,const int HEIGHT): m_Up(up),m_Eye(e),m_At(view)
{
    if( WIDTH > HEIGHT) // if the window is wider than it is tall
    {
        m_FovX = M_PI/4.0f;
        m_FovY = (1.0/ASPECT) * m_FovX;
    } else if(WIDTH <HEIGHT ) // or higher than it is wide
    {
        m_FovY = M_PI/4.0f;
        m_FovX = ASPECT* m_FovY;
    } else
    {
        m_FovX = M_PI/4.0f;
        m_FovY = M_PI/4.0f;
    }
    
    m_Right = (m_Up.crossProduct(m_Eye - m_At)).normalize();
    
    
    m_TanFovX = tan(m_FovX/2.0f);
    m_TanFovY = tan(m_FovY/2.0f);
    
    createCoordinateFrame();
}

Camera::Camera(const Vector & e,const Vector & view, const Vector & up,const FLOAT fX,const FLOAT fY):
    m_Up(up),m_Eye(e),m_At(view),m_FovX(fX),m_FovY(fY)
{
    
    m_TanFovX = tan(m_FovX/2.0f);
    m_TanFovY = tan(m_FovY/2.0f);
    
    m_Right = (m_Up.crossProduct(m_Eye - m_At)).normalize();

    createCoordinateFrame();
}

void Camera::setFov(const unsigned int WIDTH,const unsigned int HEIGHT)
{
	if( WIDTH > HEIGHT ) // if the window is wider than it is tall
    {
        m_FovX = M_PI/4.0f;
		m_FovY =((float)HEIGHT/(float)WIDTH) * m_FovX;
	} else if(WIDTH <HEIGHT ) // or higher than it is wide
    {
        m_FovY = M_PI/4.0f;
		m_FovX = ((float)WIDTH/(float)HEIGHT) * m_FovY;
    } else
    {
        m_FovX = M_PI/4.0f;
        m_FovY = M_PI/4.0f;
    }
    
    
    m_TanFovX = tan(m_FovX/2.0f);
    m_TanFovY = tan(m_FovY/2.0f);
    
	createCoordinateFrame();
}

void Camera::MoveUp(float distance)
{
    m_Eye += m_Up*distance;    
    createCoordinateFrame();
}
void Camera::MoveRight(float distance)
{
    m_Eye += m_Right * distance;
    
    createCoordinateFrame();
}


void Camera::MoveForward(float distance)
{
    Vector forward = (m_At - m_Eye).normalize();
    m_Eye += forward * distance;
    
    createCoordinateFrame();
}

void Camera::lookAt(const Vector & position)
{
    m_At = (position - m_Eye).normalize();
    createCoordinateFrame();
}



void Camera::SendToKernal(OpenCLWrapper &CL) const
{
    float eye[3] = { m_Eye.getX(),m_Eye.getY(),m_Eye.getZ() };
    CL.CreateBuffer("eye", CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY, sizeof(eye), eye);
    
    
    float U[3] = { m_U.getX(),m_U.getY(),m_U.getZ() };
    CL.CreateBuffer("U", CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY, sizeof(U), U);
    
    float V[3] = { m_V.getX(),m_V.getY(),m_V.getZ() };
    CL.CreateBuffer("V", CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY, sizeof(V), V);
    
    float W[3] = { m_W.getX(),m_W.getY(),m_W.getZ() };
    CL.CreateBuffer("W", CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY, sizeof(W), W);
    
    CL.SetKernalArgs(1,"eye");
    CL.SetKernalArgs(2,"U");
    CL.SetKernalArgs(3,"V");
    CL.SetKernalArgs(4,"W");
    CL.SetKernalArgs(5, sizeof(m_FovX),&m_FovX);
    CL.SetKernalArgs(6, sizeof(m_FovY),&m_FovY);
    
}

void Camera::createCoordinateFrame()
{
    m_W = m_At.negative().normalize();
    
    m_U = m_W.crossProduct(m_Up)*m_FovX;
    m_U = m_U.normalize();
    
    
    m_V = m_W.crossProduct(m_U)*m_FovY;
    m_V = m_V.normalize();
}


const Ray Camera::generateRay(const std::pair<FLOAT,FLOAT> & sample,const std::pair<unsigned int,unsigned int> & dimentions) const
{
    
    FLOAT alpha =  m_TanFovX * ( (sample.first  - (float)(dimentions.first/2))  / (float)(dimentions.first/2) );
    
    FLOAT beta  =  m_TanFovY * ( (sample.second - (float)(dimentions.second/2) )/(float)(dimentions.second/2));
    
    Vector direction(m_U*alpha + m_V*beta - m_W);
    direction = direction.normalize();
    
    
    return Ray(m_Eye,direction);
}