//
//  Ray.h
//  RaytracerTake2
//
//  Created by Stephen Mcgroarty on 23/11/2013.
//  Copyright (c) 2013 Stephen Mcgroarty. All rights reserved.
//

#ifndef RaytracerTake2_Ray_h
#define RaytracerTake2_Ray_h
#include "Vector.h"


class Ray
{
public:
    Ray() { }
    Ray(const Vector & origin,const Vector & direction)
    {
        this->origin = origin;
        this->direction = direction;
    }
    
    const Vector & getOrigin() const { return origin; }
    const Vector & getDirection()const { return direction;}

private:
    Vector origin,direction;
};

#endif
