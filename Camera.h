//
//  Camera.h
//  RaytracerTake2
//
//  Created by Stephen Mcgroarty on 23/11/2013.
//  Copyright (c) 2013 Stephen Mcgroarty. All rights reserved.
//

#ifndef __RaytracerTake2__Camera__
#define __RaytracerTake2__Camera__

#include <iostream>
#include "Ray.h"
#include "OpenCLWrapper.h"


class Camera
{
public:
    Camera(const Vector & eye,const Vector & view, const Vector & up,FLOAT aspect,const int WIDTH,const int HEIGHT);
    Camera(const Vector & eye,const Vector & view,const Vector & up,const FLOAT fovX,const FLOAT fovY);
    
    const Ray generateRay(const std::pair<FLOAT, FLOAT> & sample,const std::pair<unsigned int,unsigned int> & dimentions)const;

    
    
    const Vector & getEye()   const   { return m_Eye;}
    const Vector & getAt()    const   { return m_At; }
    const Vector & getUp()    const   { return m_Up; }
    
    
    const Vector & getU()   const { return m_U;}
    const Vector & getV()   const { return m_V;}
    const Vector & getW()   const { return m_W;}
    
    FLOAT getFovX()     const { return m_TanFovX; }
    FLOAT getFovY()     const { return m_TanFovY; }
    
    
    void SendToKernal(OpenCLWrapper & CL) const ;
    
    void lookAt(const Vector& position);
    
    void MoveUp(float distance);
    void MoveRight(float distance);
    void MoveForward(float distance);
	
    void setFov(const unsigned int,const unsigned int);
    
private:
    Vector m_Eye,m_At,m_Up, m_Right;
    
    
    Vector m_W,m_U,m_V;//coordinate frame
    void createCoordinateFrame();
    
    FLOAT m_FovX,m_FovY;
    FLOAT m_TanFovX,m_TanFovY;
};


#endif /* defined(__RaytracerTake2__Camera__) */
