
__constant float SHADOW_BIAS =0.01f;



typedef struct Object
{
    void * object;//The object which this is pointing at
    int type;     //The object type that the above pointer points to, 0 = sphere, 1 = plane
}Object;


typedef struct Camera
{
    float3 origin; // A.K.A Eye
    float fovx;
    float3 U,V,W;  //The basis vectors for the ray casting coordinate frame
    float fovy;
}Camera;


// An infinite 2D plane along the vector perpindicular to the normal at a distance from the origin
typedef struct Plane
{
    float3 normal;
    float distance;
    float4 colour;
    bool checkboard;
} Plane;


// A simple 3D sphere
typedef struct Sphere
{
    float3 position;
    float radius;
    float4 colour;
} Sphere;


//Simple light structure
typedef struct Light
{
    float4 position; // W component determines what type of light we are dealing with, 0 = point 1 = directional
    float intensity; //The intensity of the light
} Light;



// A structure to encapsulate the scene
typedef struct Scene
{
    Camera camera;
    Plane plane;
    Light light;
    Sphere spheres[10];       //This is the total number of spheres that can be in the scene
    unsigned int numSpheres; //Where this is the number of spheres in the scene
} Scene;



typedef struct Ray
{
    float3 direction,origin;
} Ray;



typedef struct RayStack
{
    Ray rays[MAX_DEPTH];
    unsigned int size;
}RayStack;



// Reflects vector around normal
float3 reflect(float3 vector,float3 normal)
{
    return vector - (2.0f *normal * dot(vector,normal));
}


/*
    Handles the intersection of a sphere and a ray, returns -1 if no collision took place else returns distance along ray that collision took place
 */
float sphereIntersection(Sphere sphere,struct Ray* ray)
{
    float3 toSphere = ray->origin - sphere.position; //The vector from the ray origin to the sphere
    float  B = 2*dot(ray->direction,toSphere);
    float  C = dot(toSphere,toSphere) - sphere.radius*sphere.radius;
    float discriminant = B*B - 4.0 * C;
    
    if( discriminant < 0 ) return -1;
    
    if( discriminant == 0 ) return -B/(2.0);
    return ( -B- sqrt(discriminant))/2.0;
}



/*
    Handles the intersection of a plane and a ray, returns -1 if no collision took place else returns distance along ray that collision took place
 */
float planeIntersection(Plane plane,struct Ray *ray)
{
    float DDotN = dot(ray->direction,plane.normal); // Distance dot direction
    
    if( DDotN == 0 )
        return -1;
    float t = -( dot(ray->origin,plane.normal) + plane.distance)/DDotN;
    if( t < 0 )
        return -1;
    else
        return t;
    
}



// Uses the phong shading model to shade an object
float4 phongShade(struct Scene * scene, float4 colour,float3 position,float3 normal,float3 point)
{
    float4 ouputColour = colour *0.3f; //reduce to a suitable amibient level
    
    
    float3 lightDirection;
    
    if( scene->light.position.w == 1.0) // If we are dealing with a directional light
        lightDirection = scene->light.position.xyz; // Just return the first 3 components
    else
        lightDirection = normalize(position - scene->light.position.xyz ); //otherwise get the vector from the light to the object
    
    
    lightDirection = (float3)(0.0f,0.0f,0.0f) - lightDirection; // direction *TO* the light so invert it
    
        
    
    float sDotN = dot(lightDirection,normal);
        
    sDotN = sDotN < 0 ? 0 : sDotN;
        
    ouputColour += (colour*0.8f) * sDotN; // Reduce colour to its deffuse level
        
    if( sDotN > 0 )
    {
        float3 halfway = normalize ( normalize(point) + lightDirection );
        float hDotN =  dot(halfway,normal) ;
            
        hDotN = hDotN < 0 ? 0 : hDotN;
            
        if( hDotN != 0   )
            ouputColour += colour * pow(hDotN, 64.0f);
        
    }
        
    ouputColour *= scene->light.intensity;
    
    return ouputColour;

}


/*
 * Takes in the scene, the casted ray and a pointer to an object
 * If a ray hits an object it makes the obj pointer point to that object and returns the distance along the ray the collision occurs
 */
float intersection(struct Scene *scene, struct Ray *ray,struct Object * object)
{
    float min = -1;
    float t = planeIntersection(scene->plane,ray);
    
    if( t != -1 )
    {
        min = t;
        object->type  = 1;
        object->object= &scene->plane;
    }
    
    for( unsigned int iSphere = 0; iSphere < scene->numSpheres; iSphere++)
    {
        t = sphereIntersection(scene->spheres[iSphere],ray);
        
        if( t >= 0 && ( t < min || min == -1)  )
        {
            min = t;
            object->type  = 0;
            object->object= &scene->spheres[iSphere];
        }
    }
    
    return min;
}



bool checkShadow(struct Scene * scene,float3 point)
{
    
    float3 lightDirection;
    
    if( scene->light.position.w == 1.0) // If we are dealing with a directional light
        lightDirection = scene->light.position.xyz; // Just return the first 3 components
    else
        lightDirection = point - scene->light.position.xyz; //otherwise get the vector from the light to the object
    
    lightDirection = (float3)(0.0f,0.0f,0.0f) - lightDirection; // direction *TO* the light so invert it
    lightDirection = normalize(lightDirection);
    
    Ray ray = { lightDirection,point + lightDirection*SHADOW_BIAS };
    
    
    Object temp;
    float t = intersection(scene,&ray,&temp);
    
    return t != -1;
}



//Gets the normal on object from point
float3 getNormal(struct Object * object, float3 point)
{
    if( object->type == 0 )
        return normalize(point - ((Sphere*)(object->object))->position);
    else
        return ((Plane*)(object->object))->normal;
}


//Gets the colour at a particular point in the image
float4 getColour(struct Scene * scene,struct Ray *ray,float intersectionDis, struct Object * object,unsigned int depth,struct RayStack * stack )
{
    
    float4 colour = { 0.0f,0.0f,0.0f,1.0f };                     // Set defualt colour to black
    float3 point = ray->origin + ray->direction*intersectionDis; // Get the point on "object" that the intersection occurs
    float3 normal = getNormal(object,point);                     // Get the normal from that object at that point
    
    bool shadowed =checkShadow( scene,point);
    
    if( object->type == 0)
        colour = ((Sphere*)(object->object))->colour;
    else
        colour = ((Plane*)(object->object))->colour;

    

    if( object->type == 0 ) // If a sphere
    {
        if( !shadowed ) // And not in shadow
        {
            colour = phongShade(scene,colour,((Sphere*)(object->object))->position,normal,point); // Phong shade
        }
    }
    else // Otherwise a plane
    {
        if( ((Plane*)(object->object))->checkboard == true ) // If just a checkboard tile
        {
            colour = (float4)(0.1,0.1,0.1,1.0);
            int i =  (int)point.x + (int)point.z;
        
            if( i % 2 )
                colour = (float4)(0.9,0.9,0.9,1.0); //slightly less than "true" white to leave "room" for the reflection
        }
    }
    
    if( !shadowed )
    {
        //Pushes the reflection ray onto the ray stack
        Ray newRay ={ reflect(ray->direction,normal),point + normal * SHADOW_BIAS };
        stack->rays[stack->size++] = newRay;
    }else
        colour *= 0.5f;
    
    
    return colour;
}





// Casts a ray from the camera
Ray castRay(Camera camera, float2 position,int2 dimentions)
{
    float alpha = camera.fovx * ((position.x - (float)(dimentions.x/2.0f))/(float)(dimentions.x/2.0f));
    float beta  = camera.fovy * ((position.y - (float)(dimentions.y/2.0f))/(float)(dimentions.y/2.0f ));
    
    float3 direction = camera.U*alpha + camera.V*beta - camera.W;
    direction = normalize(direction);
    
    Ray ray = { direction, camera.origin };
  
    return ray;
    
}




Scene createScene(__global float* cameraOrigin,
                  __global float *U,
                  __global float *V,
                  __global float *W,
                  float fovx,
                  float fovy,
                  float dx)
{
    
    
    Scene scene;
    scene.camera.origin = vload3(0,cameraOrigin);
    scene.camera.U = vload3(0,U);
    scene.camera.V = vload3(0,V);
    scene.camera.W = vload3(0,W);
    scene.camera.fovx = fovx;
    scene.camera.fovy = fovy;


    Plane plane =  { (float3)(0.0f,1.0f,0.0f), //normal
                      1.0f,                   //Distance
                     (float4)(0.25f,0.25f,0.25f,1.0f), //Colour
                    true } ; //Checkboard
    scene.plane = plane;
    
    scene.spheres[0].position = (float3)(0,dx,0);
    scene.spheres[0].radius   = 1.0f;
    scene.spheres[0].colour   = (float4)(1.0f,0.0f,0.0f,1.0f);
    
    
    scene.spheres[1].position = (float3)(2.0f,0.0f,2.0f);
    scene.spheres[1].radius   = 1.0f;
    scene.spheres[1].colour   = (float4)(0.0f,0.0f,1.0f,1.0f);
    
    
    scene.spheres[2].position = (float3)(-2,0,-1.0f);
    scene.spheres[2].radius   = 0.5f;
    scene.spheres[2].colour   = (float4)(1.0f,1.0f,1.0f,1.0f);
    
    scene.spheres[3].position = (float3)(2.0,0.5f,-2.0f);
    scene.spheres[3].radius   = 1.5f;
    scene.spheres[3].colour   = (float4)(0.0f,1.0f,0.0f,1.0f);
    
    scene.spheres[4].position = (float3)(-2.0f,0.5f,1.0f);
    scene.spheres[4].radius   = 0.3f;
    scene.spheres[4].colour   = (float4)(0.0f,0.0f,0.0f,1.0f);
    
    scene.spheres[5].position = (float3)(0.0f,5.0,-10.0f);
    scene.spheres[5].radius   = 5.0f;
    scene.spheres[5].colour   = (float4)(0.8f,0.6f,0.0f,1.0f);
    
    
    scene.spheres[6].position = (float3)(-2.5f,0.0f,-2.5f);
    scene.spheres[6].radius   = 1.0f;
    scene.spheres[6].colour   = (float4)(1.0f,0.0f,1.0f,1.0f);
    
    scene.spheres[7].position = (float3)(3.5f,3.0,0.0f);
    scene.spheres[7].radius   = 2.0f;
    scene.spheres[7].colour   = (float4)(1.0f,0.0f,1.0f,1.0f);
    
    
    scene.spheres[8].position = (float3)(-4.5f,0.0,0.0f);
    scene.spheres[8].radius   = 1.0f;
    scene.spheres[8].colour   = (float4)(1.0f,1.0f,0.0f,1.0f);
    
    scene.spheres[9].position = (float3)(-3.5f,3.0,-3.0f);
    scene.spheres[9].radius   = 1.5f;
    scene.spheres[9].colour   = (float4)(1.0f,0.0f,0.0f,1.0f);
    
    scene.numSpheres = 10;
    

    
    scene.light.position =(float4)(1.0f,-1.0f,-1.0f,1.0f) ;
    scene.light.intensity = 0.8f;

    return scene;
}


__kernel void Raytracer(__write_only image2d_t outputImage,
                        __global float* cameraOrigin,
                        __global float *U,
                        __global float *V,
                        __global float *W,
                        float fovx,
                        float fovy,
                        float dx,
                        int MSAA)
{
    int x = get_global_id(0); // x pixel
    int y = get_global_id(1); // y pixel
        
    Scene scene = createScene(cameraOrigin,U,V,W,fovx,fovy,dx);
    
    float4 colour= (float4)(0.0f,0.0f,0.0f,0.0f);
    
    
    int2 dimentions = {get_global_size(0),get_global_size(1)};

    for(int sampleX= -MSAA/2; sampleX < MSAA/2; sampleX++)
    {
        for(int sampleY =-MSAA/2; sampleY < MSAA/2; sampleY++)
        {
            // Get the UV coords of the image
            float2 position = {(float)x+ (((float)sampleX +0.5f)/(float)MSAA),
                (float)y+(((float)sampleY +0.5f)/(float)MSAA)};
            
            RayStack stack;
            stack.size = 0;
            stack.rays[stack.size++] = castRay(scene.camera,position,dimentions);

            
            
            for(unsigned int stackSize = 0; stackSize < stack.size && stackSize < MAX_DEPTH; stackSize++)
            {
                Object returnee;
                float intersectionPoint = intersection(&scene,&stack.rays[stackSize],&returnee);

                if( intersectionPoint > 0 )
                    colour += getColour(&scene,&stack.rays[stackSize],intersectionPoint,&returnee,stackSize,&stack);
            }
        }
    }
    colour /= (float)(MSAA*MSAA);

    
    write_imagef(outputImage,(int2)(x,y),colour);
}