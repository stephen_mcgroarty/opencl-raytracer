//
//  Common.h
//  OpenCLRaytracerTest
//
//  Created by Stephen Mcgroarty on 13/03/2014.
//  Copyright (c) 2014 Stephen Mcgroarty. All rights reserved.
//

#ifndef OpenCLRaytracerTest_Common_h
#define OpenCLRaytracerTest_Common_h

typedef float FLOAT;

typedef unsigned char Byte;

struct Colour{
    Byte red,green,blue,alpha;
};

const std::string FILE_PATH = "/Users/Stephen/Documents/C++ Projects/OpenCLRaytracerTest/OpenCLRaytracerTest/";



#endif
