//
//  OpenCLWrapper.cpp
//  OpenCL
//
//  Created by Stephen Mcgroarty on 06/03/2014.
//  Copyright (c) 2014 Stephen Mcgroarty. All rights reserved.
//

#include "OpenCLWrapper.h"
#include <iostream>
#include <fstream>


OpenCLWrapper::~OpenCLWrapper()
{
    for(auto iBuffer = m_Buffers.begin(); iBuffer != m_Buffers.end(); ++iBuffer)
    {
        clReleaseMemObject (iBuffer->second);
    }
	//clReleaseCommandQueue (m_CommandQueue);
    
    for(auto iQueue = m_Queues.begin(); iQueue != m_Queues.end(); iQueue++)
        clReleaseCommandQueue (iQueue->second);

    m_Queues.clear();
    clReleaseKernel (m_Kernel);
	clReleaseProgram (m_Program);
	clReleaseContext (m_Context);
}


std::string OpenCLWrapper::GetPlatformInfo(unsigned int index, cl_platform_info infoParam) const
{
    std::string data;
    size_t size =0;
    clGetPlatformInfo(m_vPlatforms[index], infoParam, 0, nullptr, &size);
    data.resize(size);
    clGetPlatformInfo(m_vPlatforms[index], infoParam, size,const_cast<char*> (data.data ()), nullptr);
    return data;
}


void *OpenCLWrapper::GetDeviceInfo(unsigned int index, cl_device_info param) const
{
    
    size_t size =0;
    clGetDeviceInfo(m_vDevices[index], param, 0, nullptr, &size);
    void * data = malloc(size);
    clGetDeviceInfo(m_vDevices[index], param, size,data,nullptr);
    return data;
}

void *OpenCLWrapper::GetEventInfo(cl_event *event, cl_event_info param) const
{
    
    size_t size =0;
    clGetEventInfo(*event, param, 0, nullptr, &size);
    void * data = malloc(size);
    clGetEventInfo(*event, param, size,data,nullptr);
    return data;
}




void OpenCLWrapper::CreateImage2D(const std::string &name, cl_mem_flags flags, size_t width, size_t height, void *data)
{
    cl_int error = CL_SUCCESS;
    const cl_image_format format[] = { CL_RGBA,CL_UNORM_INT8};
    const cl_image_desc discription[] = { CL_IMAGE_2D,width,height,0 };
    
    
    m_Buffers[name] = clCreateImage(m_Context, flags, format, discription, data, &error);
    if( error != CL_SUCCESS ) printf("Error %i creating image named %s\n",error,name.c_str());
}


void OpenCLWrapper::Init()
{
    unsigned int numPlatforms = 0;
    clGetPlatformIDs(0, nullptr, &numPlatforms);
    m_vPlatforms.resize(numPlatforms);
    clGetPlatformIDs(numPlatforms, &m_vPlatforms[0], nullptr);
    

    for(unsigned int iPlatform =0; iPlatform < m_vPlatforms.size(); ++iPlatform)
    {
        printf("Platform number %i:\n",iPlatform);
        printf("    Platform name: %s\n",       GetPlatformInfo(iPlatform,CL_PLATFORM_NAME).c_str());
         //printf("    Platform profile: %s\n",    GetPlatformInfo(iPlatform,CL_PLATFORM_PROFILE).c_str());
        //printf("    Platform extensions: %s\n\n", GetPlatformInfo(iPlatform, CL_PLATFORM_EXTENSIONS).c_str());
    }
    
    
    
    unsigned int numDevices = 0;
    clGetDeviceIDs(m_vPlatforms[0], CL_DEVICE_TYPE_ALL, 0, nullptr, &numDevices);
    m_vDevices.resize(numDevices);
    clGetDeviceIDs(m_vPlatforms[0], CL_DEVICE_TYPE_ALL, numDevices, &m_vDevices[0], nullptr);
    
    for(unsigned int iDevice = 0; iDevice < numDevices; iDevice++)
    {
        cl_device_type * device = static_cast<cl_device_type*>(GetDeviceInfo(iDevice, CL_DEVICE_TYPE));
       
        if( *device == CL_DEVICE_TYPE_GPU )
            m_MainGPU = iDevice;
        else if( *device == CL_DEVICE_TYPE_CPU)
            m_MainCPU = iDevice;
        free(device);
    }
    
        
    for(unsigned int iDevice =0; iDevice < m_vDevices.size(); iDevice++)
    {
        printf("Device number %i:\n",iDevice);
        
        char * ch = static_cast<char*>(GetDeviceInfo(iDevice, CL_DEVICE_NAME));
        printf("    Device name: %s\n",ch);
        free(ch);
    }
    
    cl_int error =0;
    
    m_Context = clCreateContext(nullptr, numDevices, &m_vDevices[0], nullptr, nullptr, &error);
    if( error != CL_SUCCESS ) printf("Error code: %i creating context\n",error);


}



void OpenCLWrapper::CreateQueue(const std::string &name, Type type)
{
    unsigned int index = 0;
    
    if(type==GPU) index = m_MainGPU;
    else index = m_MainCPU;
    
    cl_int error =0;
    m_Queues[name] = clCreateCommandQueue(m_Context, m_vDevices[index], 0, &error);
    if( error != CL_SUCCESS ) printf("Error code %i command queue\n",error);
}



void OpenCLWrapper::SetKernalArgs(unsigned int index, size_t size,const void *data)
{
    cl_int error = clSetKernelArg(m_Kernel, index, size, data);
    if( error != CL_SUCCESS ) printf("Error %i setting kernel arg index %i to data of size %zu\n",error,index,size);

}

void OpenCLWrapper::SetKernalArgs(unsigned int index, const std::string &buffer)
{
    cl_int error = clSetKernelArg(m_Kernel, index, sizeof(cl_mem), &m_Buffers[buffer]);
    if( error != CL_SUCCESS ) printf("Error %i setting kernel arg index %i to buffer: %s\n",error,index,buffer.c_str());

}

void OpenCLWrapper::LoadProgram(const std::string & filepath,const char * args)
{
    std::ifstream source(filepath.c_str(), std::ios::binary | std::ios::ate| std::ios::in);
	
    if( source.is_open() )
    {
        size_t length = source.tellg();
        source.seekg(source.beg);
        
        char * str = new char[length+1];
        str[length] = '\0';
        source.read(str,length);
        source.close();
        
        m_ProgramSource = str;
        
        cl_int error = CL_SUCCESS;
        
        
        const char * src = str;
        m_Program = clCreateProgramWithSource(m_Context, 1, &src, &length, &error);
        if( error != CL_SUCCESS ) printf("Error code %i thrown when creating program\n",error);
        
        
        error = clBuildProgram(m_Program, m_vDevices.size(), &m_vDevices[0],args, nullptr, nullptr);
        
        if( error != CL_SUCCESS)
        {
            printf("Error code %i thrown when building program\n",error);
            cl_build_status status;
            
            clGetProgramBuildInfo(m_Program, m_vDevices[0], CL_PROGRAM_BUILD_STATUS, sizeof(status), &status, nullptr);
            if( status != CL_BUILD_NONE )
            {
                size_t len;
                clGetProgramBuildInfo(m_Program, m_vDevices[0], CL_PROGRAM_BUILD_LOG, 0, nullptr, &len);
                

                char * errorLog = (char*)malloc(len);
                clGetProgramBuildInfo(m_Program, m_vDevices[0], CL_PROGRAM_BUILD_LOG, len, errorLog, nullptr);
                printf("%s\n",errorLog);
                free(errorLog);
            }
        }
        
        m_Kernel = clCreateKernel(m_Program, "Raytracer", &error);
        if( error != CL_SUCCESS ) printf("Error code %i thrown creating kernal\n",error);
        
        delete[] str;
    } else
    {
        printf("Error no file named: %s was found\n",filepath.c_str());
        
    }
}


void OpenCLWrapper::CreateBuffer(const std::string &name, cl_mem_flags flags, unsigned int size, void *data)
{
    cl_int error = CL_SUCCESS;
    
    if( m_Buffers.find(name) != m_Buffers.end() ) //If buffer already has been created
        clReleaseMemObject(m_Buffers[name]);      //Release previously assigned memory
    
    m_Buffers[name] = clCreateBuffer(m_Context, flags, size, data, &error);
    
    if( error != CL_SUCCESS ) printf("Error %i creating buffer\n",error);

}

void OpenCLWrapper::WriteBuffer(const std::string & queue,const std::string &name, unsigned int size, const void *data,bool blocking)
{
    cl_int error = clEnqueueWriteBuffer(m_Queues[queue], m_Buffers[name], blocking, 0, size, data, 0, nullptr, nullptr);
    if( error != CL_SUCCESS ) printf("Error %i writing to buffer\n",error);

}

void *OpenCLWrapper::ReadBuffer(const std::string & queue,const std::string &name, unsigned int size,bool blocking)
{
    void * data = malloc(size);
    
    cl_int error = clEnqueueReadBuffer(m_Queues[queue], m_Buffers[name], blocking, 0, size, data, 0, nullptr, nullptr);
    if( error != CL_SUCCESS ) printf("Error %i reading from buffer\n",error);

    return data;
}

void OpenCLWrapper::EnqueueNDRangeKernel(const std::string & queue,unsigned int dimentions,const size_t globalWorkSize[3],const size_t localWorkSize[3],cl_event* event)
{
    clEnqueueNDRangeKernel(m_Queues[queue], m_Kernel, dimentions, 0, globalWorkSize, localWorkSize, 0, nullptr, event);
}

void* OpenCLWrapper::ReadImage(const std::string & queue,const std::string &name, const size_t offset[3], const size_t region[3], bool blocking)
{
    void * data = malloc(region[0] * region[1] * region[2] );
    
    cl_int error = clEnqueueReadImage(m_Queues[queue], m_Buffers[name], blocking, offset, region, 0, 0, data, 0, nullptr, nullptr);
    
    if( error != CL_SUCCESS ) printf("Error %i reading from image named %s\n",error,name.c_str());
    
    return data;
}


void OpenCLWrapper::ReadImage(const std::string & queue,
                              const std::string &name,
                              const size_t offset[3],
                              const size_t region[3],
                              void * data,
                              bool blocking,
                              const cl_event* list,
                              unsigned int numEventsInList,
                              cl_event * event)
{
    
    cl_int error = clEnqueueReadImage(m_Queues[queue], m_Buffers[name], blocking, offset, region, 0, 0, data, numEventsInList, list, event);
    if( error != CL_SUCCESS ) printf("Error %i reading from image named %s\n",error,name.c_str());
    
    
}

